Name:		bughunting
Version:	2017.0
Release:	2%{?dist}
Summary:	Red Hat Bughunting Competition client tools

License:	MIT
URL:		http://www.redhat.com
Source0:	%{name}-%{version}.tar.xz

BuildArch:	noarch

BuildRequires: ruby-devel
Requires: ruby(runtime_executable) >= 1.8
Requires: rubygem(term-ansicolor)
Requires: sudo

%description

%package client
Summary:	Red Hat Bughunting Competition client tools
Requires:	%{name} = %{version}-%{release}

%description client

%package server
Summary:	Red Hat Bughunting Competition server and development tools
Requires(pre):	shadow-utils
Requires(post):	openssh
Requires:	%{name} = %{version}-%{release}
Requires:	python, PyYAML
Requires:	ruby
%if 0%{?fedora} > 0
# TODO: should be fixed - but we run the production server on Fedora, so this is
# not an issue ATM.  This %%if fork is here to allow installing
# bughunting-server package on RHEL machines to do testing by huntlocal.
Requires:       rubygem(sinatra)
%endif
Requires:       rubygem(sqlite3)
Requires:       rubygem(json)
Requires:       rubygem(haml)

%description server


%prep
%setup -q


%build


%install
make install DESTDIR=%{buildroot}

# create directory for SSH keys and touch files generated during installation
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
touch %{buildroot}%{_sysconfdir}/%{name}/remote_id_rsa
touch %{buildroot}%{_sysconfdir}/%{name}/remote_id_rsa.pub


%pre server
# users for jail (not the users that are on clients or client machine names)
for user in bughunting{1..16} ; do
  getent passwd $user >/dev/null || useradd -M -d /dev/null -s /sbin/nologin $user
done

%post server
# server installation
if [ "$1" -eq 1 ]; then
    # lets generate SSH keys
    ssh-keygen -f %{_sysconfdir}/%{name}/remote_id_rsa -t rsa -N '' -C bughunting > /dev/null
fi


%files
%doc README
%dir %{_sharedstatedir}/bughunting
%dir %{_sysconfdir}/bughunting
%dir %{ruby_vendorlibdir}/bughunting
%dir %{_datadir}/bughunting
%{ruby_vendorlibdir}/bughunting/common/
%{ruby_vendorlibdir}/bughunting/common.rb


%files client
%dir %{_sharedstatedir}/bughunting/cache
%config(noreplace) %{_sysconfdir}/bughunting/client.yml
%{ruby_vendorlibdir}/bughunting/client/
%{_bindir}/hunt


%files server
%dir %{_sharedstatedir}/%{name}/tasks
%dir %{_sharedstatedir}/%{name}/archive
%config(noreplace) %{_sysconfdir}/%{name}/server.yml
%ghost %config(noreplace) %{_sysconfdir}/%{name}/remote_id_rsa
%ghost %config(noreplace) %{_sysconfdir}/%{name}/remote_id_rsa.pub
%{ruby_vendorlibdir}/%{name}/check/
%{ruby_vendorlibdir}/%{name}/devel/
%{ruby_vendorlibdir}/%{name}/install/
%{ruby_vendorlibdir}/%{name}/server/
%{_bindir}/huntserver
%{_bindir}/huntinstall
%{_bindir}/huntlocal
%{_datadir}/%{name}/deploy-clients.sh
%{_datadir}/%{name}/reset-clients.sh
%{_datadir}/%{name}/get-clients.py*

#web part
%config(noreplace) %{_sysconfdir}/bughunting/web.yml
%{_datadir}/%{name}/web
%{ruby_vendorlibdir}/%{name}/web/


%changelog
* Thu Mar 23 2017 Pavel Raiskup <praiskup@redhat.com> - 2017.0-2
- require rubygem haml

* Thu Mar 16 2017 Tomas Hozza <thozza@redhat.com> - 2017.0-1
- bumped version for 2017
- generate SSH RSA keys when server is installed
- minor SPEC edits to use macros instead of hardcoded values

* Wed Apr 06 2016 Pavel Raiskup <praiskup@redhat.com> - 2016.0-6
- bump version for client? fixes

* Wed Mar 09 2016 Pavel Raiskup <praiskup@redhat.com> - 2016.0-5
- fix installation on RHEL7

* Wed Mar 09 2016 Michal Luscon <mluscon@redhat.com> - 2016.0-3
- add bughunting requirement for server subpackage

* Wed Mar 09 2016 Pavel Raiskup <praiskup@redhat.com> - 2016.0-2
- new version FY2016
- don't require non-existing web subpackage

* Sun Mar 29 2015 Pavel Raiskup <praiskup@redhat.com> - 2015.0-2
- resolve rhbz#1167767 issues

* Thu Mar 19 2015 Michal Luscon <mluscon@redhat.com> - 2015.0-1
- new version 2015

* Wed Aug 27 2014 Michal Luscon <mluscon@redhat.com> - 2014.0-7
- merge bughunting-server and bughunting-web packages

* Fri Apr 04 2014 Pavel Raiskup <praiskup@redhat.com> - 2014.0-6
- from now deploy general dependencies via ks

* Wed Apr 02 2014 Pavel Raiskup <praiskup@redhat.com> - 2014.0-5
- install manuals on client

* Mon Mar 31 2014 Pavel Raiskup <praiskup@redhat.com> - 2014.0-4
- huntserver does not work without *-web package

* Sat Mar 29 2014 Pavel Raiskup <praiskup@redhat.com> - 2014.0-3
- install "library" file needed for bug-hunters confusion

* Fri Mar 07 2014 Michal Luscon <mluscon@redhat.com> - 2014.0-2
- huntinstall: use task directory from server config
- update INSTALL and README

* Wed Jan 29 2014 Pavel Raiskup <praiskup@redhat.com> - 2014.0-1
- try to init for 2014 round

* Mon Apr 08 2013 Pavel Raiskup <praiskup@redhat.com> - 2013.0-1
- fix ruby requirement (I'm not ruby guru but according to fedora guidelines
  this should be ok)

* Wed Apr 04 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-10
- server fixes

* Wed Apr 04 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-9
- client fixes, qa tasks evaluation fixes

* Wed Apr 04 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-8
- testround 2 fixes

* Tue Apr 03 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-7
- testround 1 fixes

* Tue Apr 03 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-6
- client fixes

* Tue Apr 03 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-5
- update config files

* Tue Apr 03 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-4
- subpackage components

* Fri Mar 23 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-3
- remove tasks subpackage, will be supplied separately

* Wed Mar 21 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-2
- some updates in local tools

* Tue Mar 20 2012 Jan Vcelak <jvcelak@redhat.com> 2012.0-1
- initial release for 2012
