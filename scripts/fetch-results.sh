#!/bin/bash

# This script fetches results from Bug Hunting runs that are backed-up
# by the script and available as compressed tar balls
# Example of usage: [this_script] bh-game-2014-04-09_1507.tar.xz 

die() {
    echo "$@" >&2
    exit 1
}

if [ $# -le 1 ] ; then
  echo "Usage: `basename $0` tasks_directory bh-game-YYYY-MM-DD_hhmm.tar.xz [ bh-game-YYYY-MM-DD_hhmm.tar.xz ... ]"
  exit 1
fi

TASKS_DIR="$1" ; shift
TMPDIR=`mktemp -d /tmp/bhfetch.XXXXXX`
workingdir=$TMPDIR/workingdir
players_all_csv=$TMPDIR/players_all.csv
scores_all_csv=$TMPDIR/scores_all.csv
newdbfile="$TMPDIR/results-all.db"

# temporary csv files with results from all runs
rm -f $players_all_csv $scores_all_csv
touch $players_all_csv $scores_all_csv

# dummy counter for runs
i=1

# extract data from all backed-up tar balls
while [ -n "$1" ] ; do
  echo "Getting users and score for run backed-up like $1"

  backuparchive="`readlink -f $1`"
  dbfile="./home/mluscon/projects/bughunting-2014/results"

  mkdir $workingdir
  echo "Working dir is $workingdir"
  pushd $workingdir >/dev/null

  echo -n "Unpacking $backuparchive ..."
  tar -xvf $backuparchive >/dev/null
  chmod -R u+w .
  echo " DONE"

  echo -n "Retrieving players from DB ..."
  [ -r $dbfile ] || die "file $dbfile not read-able."
  sqlite3 $dbfile >>$players_all_csv <<EOF
.mode csv
select email, name, $i from players;

EOF
  echo " DONE"

  echo -n "Retrieving scores from DB ..."
  sqlite3 $dbfile >>$scores_all_csv <<EOF
.mode csv
select
  p.email, s.task, s.score
from
  scores as s
left join players as p on p.player LIKE s.player;

EOF
  echo " DONE"

  popd >/dev/null
  echo "Removing working dir $workingdir"
  rm -rf $workingdir
  shift
  ((i++))
done

echo -n "Preparing tables for final results ..."
sqlite3 $newdbfile &>/dev/null <<'EOF'
  DROP TABLE players;
  DROP TABLE scores;

EOF
sqlite3 $newdbfile <<EOF
  CREATE TABLE scores (
    email VARCHAR NOT NULL,
    task VARCHAR NOT NULL,
    score TINYINT NOT NULL,
    UNIQUE (email, task)
  );
  CREATE TABLE players (
    email VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    round TINYINT NOT NULL
  );
.separator ","
.import $players_all_csv players
.import $scores_all_csv scores

EOF
echo " Done"
echo $newdbfile

get_all_tasks() {
  pushd $1 >/dev/null
  grep language */task.yml |grep -v SAMPLE|cut -d/ -f1|sort|uniq 2>/dev/null
  popd >/dev/null
}

# This is really ugly way how to get final SQL query from tasks git
get_sql_query() {
  export TASKS_DIR=$TASKS_DIR
  SELECT_FILE=`mktemp .select.XXXXXX`
  JOIN_FILE=`mktemp .select.XXXXXX`
  i=1
  touch $SELECT_FILE $JOIN_FILE
  pushd $TASKS_DIR >/dev/null
  for task in `get_all_tasks $TASKS_DIR` ; do
    l="`grep -e 'language:' $task/task.yml | cut -d: -f2`" ; l=`echo $l`
    echo "${l}:$task"
  done | grep -v SAMPLE | sort | while read line ; do
    l=`echo $line|cut -d: -f1`
    task=`echo $line|cut -d: -f2`
    task_col=`echo $task|sed -e 's/-/_/g'`
    echo "Generating SQL using ... lang: $l, task: $task, task for column name: $task_col (loop $i)" >&2
    ((i++))
    echo ", CASE WHEN s$i.score is NULL THEN 0 ELSE s$i.score END as '${l}_$task_col'" >>$SELECT_FILE
    echo " LEFT JOIN scores AS s$i ON p.email LIKE s$i.email AND s$i.task LIKE '$task'" >>$JOIN_FILE
  done
  echo "SELECT p.name, p.email, p.round"
  cat $SELECT_FILE
  echo "FROM players AS p"
  cat $JOIN_FILE
  echo ";"
  popd >/dev/null
}

echo "SQL query generated from tasks directory $TASKS_DIR:"


results_all_csv=results_all.csv
echo -n "Generating final CSV file $results_all_csv ..."
rm -f $results_all_csv ; touch $results_all_csv
full_results_all_csv=$(readlink -f $results_all_csv)
pushd $TASKS_DIR >/dev/null
echo -n "name,email,round" >>$full_results_all_csv
for task in `get_all_tasks $TASKS_DIR` ; do
    l="`grep -e 'language:' $task/task.yml | cut -d: -f2`" ; l=`echo $l`
    echo "${l}:$task"
  done | grep -v SAMPLE | sort | while read line ; do
    l=`echo $line|cut -d: -f1`
    task=`echo $line|cut -d: -f2`
    task_col=`echo $task|sed -e 's/-/_/g'`
    echo -n ",${l}_${task_col}" >>$full_results_all_csv
done
echo >>$full_results_all_csv
popd >/dev/null
sqlite3 $newdbfile >>$results_all_csv <<EOF
.mode csv
`get_sql_query $TASKS_DIR`

EOF

echo " DONE"
echo "Results are in $results_all_csv"

