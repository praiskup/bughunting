#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/common"

require "thread"

class Users
  def initialize(users = [])
    @users = users.clone
    @free_users = @users.clone
    @running_users = []

    @mutex = Mutex.new
  end

  def get_free(ip)
    user = nil
    @mutex.synchronize do
      next if has_running_test? ip
      next if @free_users.empty?
      user = add_running(ip)
    end
    user
  end

  def return(ip)
    @mutex.synchronize do
      remove_running(ip)
    end
  end

  private

  def has_running_test?(ip)
    @running_users.each do |running|
      return true if running[:ip] == ip
    end
    false
  end

  def add_running(ip)
    reservation = {
      :ip   => ip,
      :user => @free_users.shift
    }
    @running_users.push reservation
    reservation[:user]
  end

  def remove_running(ip)
    user = nil
    @running_users.each_with_index do |reservation, index|
      if reservation[:ip] == ip
        user = reservation[:user]
        @running_users.delete_at index
        break
      end
    end

    unless user.nil?
      @free_users.unshift user
    end
  end
end

