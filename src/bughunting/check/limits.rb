#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

class Limits
  attr_accessor :time, :memory, :process_count

  def initialize(time = nil, memory = nil, process_count = nil)
    @time = time
    @memory = memory
    @process_count = process_count
  end

  def ulimit_command
    options = []

    options << "-t %d" % @time unless @time.nil?
    options << "-m %d" % @memory unless @memory.nil?
    options << "-u %d" % @process_count unless @process_count.nil?

    if options.empty?
      return nil
    else
      return "ulimit " + options.join(" ")
    end
  end

  def self.from_config(server_config)
    Limits.new \
      server_config["limits.cputime_seconds"],
      server_config["limits.memory_megabytes"],
      server_config["limits.process_count"]
  end
end

