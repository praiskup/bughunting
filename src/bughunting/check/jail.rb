#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/check/log"
require "tmpdir"

class Jail
  attr_reader :path, :exec_dir, :check_logfile, :log

  def initialize(owner_uid)
    @owner_uid = owner_uid
  end

  def create(task, carrier)
    init_dir
    @log = Log.new(File.join @path, "jail.log")
    copy_player_files(task, carrier) || raise("Cannot copy checked files. You have moved them, haven't you?")
    copy_check_files(task) || raise("Cannot setup check scripts. Call for help.")
    set_permissions
  end

  def destroy
    return if @path.nil?
    @log.close
    FileUtils.remove_entry_secure @path
    @path = nil
  end

  def set_world_readable
    system "chmod", "-R", "a=rX", @path
  end

  def archive(target_dir)
    return if @path.nil?
    @log.close
    system "chown", "-R", "%d:%d" % [Process.uid, Process.gid], @path
    system "chmod", "-R", "u=rX,g=rX,o=", @path
    system "mv", @path, target_dir
    @path = nil
  end

  private

  def init_dir
    @path = Dir.mktmpdir "bughunting"
    FileUtils.chmod 0711, @path

    @exec_dir = File.join @path, "work"
    Dir.mkdir @exec_dir, 0700

    @check_logfile = File.join @path, "check.log"
    File.new(@check_logfile, "w").close
  end

  def copy_player_files(task, carrier)
    @log.msg "copying player files"
    carrier.get @exec_dir, task.config["check_files"]
  end

  def copy_check_files(task)
    @log.msg "copying check files"
    task_dir = File.expand_path task.dir
    check_dir = File.join task_dir, "check"
    check_files = Dir.glob("%s/*" % check_dir)
    FileUtils.cp_r check_files, @exec_dir
  end

  def set_permissions
    @log.msg "fixing permissions"
    FileUtils.chown_R @owner_uid, nil, @exec_dir
    FileUtils.chown @owner_uid, nil, @check_logfile
  end
end
