#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++


class CarrierFactory
  def initialize(remote_sources_root)
    @remote_sources_root = remote_sources_root
  end

  def build_local(source_dir)
    LocalCarrier.new source_dir
  end

  def build_remote(user, host, identity_file, sources_dir, task_name)
    RemoteCarrier.new user, host, identity_file, @remote_sources_root, task_name
  end
end

require "fileutils"

class LocalCarrier
  def initialize(source_dir)
    @source_dir = source_dir
  end

  def get(target_dir, source_files)
    unless File.directory? target_dir
      raise "Target dir is not a directory"
    end

    source_files.each do |filename|
      filename_base = File.basename filename
      full_source = File.join @source_dir, filename
      full_target = File.join target_dir, filename_base

      FileUtils.cp_r full_source, full_target
    end
  end
end

class RemoteCarrier
  def initialize(user, host, identity_file, sources_root, task_name)
    @user = user
    @host = host
    @identity_file = identity_file
    @sources_root = sources_root
    @task_name = task_name
  end

  def get(target_dir, source_files)
    system *command(target_dir, source_files)
  end

  private

  def command(target_dir, source_files)
    [
      "/usr/bin/scp",
      "-pr", # preserve attributes, recursive
      "-o", "BatchMode=yes",
      "-o", "StrictHostKeyChecking=no",
      "-i", @identity_file,
      sources(source_files),
      target_dir
    ]
  end

  def sources(source_files)
    sources = "#{@user}@#{@host}:#{@sources_root}/#{@task_name}/"
    if source_files.size == 1
      sources += source_files[0]
    else
      sources += "{%s}" % source_files.join(",")
    end
    sources
  end
end
