#!/usr/bin/ruby


require "bughunting/common"
require "bughunting/install/controller"
require "rubygems"
require "optparse"

server_config = "/etc/bughunting/server.yml"
web_config = "/etc/bughunting/web.yml"
debug = false

parser = OptionParser.new do |opts|
  opts.banner = \
    "Usage: #{$PROGRAM_NAME} [--debug] hostname\n" \
    "       #{$PROGRAM_NAME} [--debug] all\n\n"

  opts.on(nil, "--debug", "Print debugging info") do |value| debug = true end
end.parse!

hostname = ARGV.pop
if hostname.nil?
  $stderr.puts "%s: %s" % ["ERROR".bold.red, "No hostname given."]
  parser.display
  exit 1
end

controller = InstallController.new(server_config, web_config, debug)

if hostname == "all"
  controller.install_all
else
  controller.install hostname
end
exit 0
