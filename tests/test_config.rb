#!/usr/bin/ruby

require "test/unit"
require "bughunting/config/base"

class TestConfigNode < Test::Unit::TestCase
  def test_nils
    config = BaseConfig.new
    assert_nil config["a"]
    assert_nil config["a.b"]
    assert_nil config["a.b.c"]
  end

  def test_levels
    config = BaseConfig.new({
      "a" => {
        "b" => {
          "c" => "hello"
        },
        "x" => 42
      },
      "y" => [1,2,3,4,5]
    })

    assert_equal [1,2,3,4,5], config["y"]
    assert_equal 42, config["a.x"]
    assert_equal "hello", config["a.b.c"]

    assert_nil config["X"]
    assert_nil config["a.X"]
    assert_nil config["a.b.X"]
    assert_nil config["a.b.c.X"]
    assert_nil config["a.b.c.d.X"]
    assert_nil config["a.b.c.d.X"]
    assert_nil config["a.x.X"]
    assert_nil config["y.X"]
  end

  def test_set
    config = BaseConfig.new

    config["a"] = "hello"
    assert_equal "hello", config["a"]

    config["b.c"] = "hi"
    assert_equal "hi", config["b.c"]

    assert_raise Exception do
      config["b.c.x"] = "foo"
    end
  end

  def test_replace
    config = BaseConfig.new

    config["a"] = "hello"
    config["a"] = "world"
    assert_equal "world", config["a"]

    config["a"] = { "c" => true }
    config["a"] = {}
    assert_equal({}, config["a"])
  end

  def test_merge
    config = BaseConfig.new({ "1" => "hello", "2" => { "a" => "A", "b" => "B" }, "3" => [1,2,3] })
    config.merge!({ "3" => "world", "4" => "new", "2" => { "a" => "rewrite", "c" => "newnew"  } })

    assert_equal "hello", config["1"]
    assert_equal "rewrite", config["2.a"]
    assert_equal "B", config["2.b"]
    assert_equal "newnew", config["2.c"]
    assert_equal "world", config["3"]
    assert_equal "new", config["4"]
  end
end
