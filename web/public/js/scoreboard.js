var ANIMATION_SPEED = 1000;
var UPDATE_DELAY = 5000;

$(document).ready(function() {
  update_scores();
});

function update_scores()
{
  $.ajax({
    url: "?json"
  }).done(function (data) {
    var results = jQuery.parseJSON(data);
    var max_score = results.max_score > 6 ? results.max_score : 6;

    for (var i = 0; i < results.ranking.length; i++) {
      var player = results.ranking[i];
      update_player(i, player.player, player.name, player.score, max_score);
    }
    setTimeout(update_scores, UPDATE_DELAY);
  });
}

function update_player(ranking, player_id, name, score, max_score)
{
  search_id = ("#player["+player_id+"]").replace(/([\.\[\]])/g, "\\$1");
  var player = $(search_id);
  if (player.length == 0)
    player = create_player(player_id, name);

  var graph_width_pc = score/max_score * 100.0 + "%";
  var top_px = ranking * 40 + "px";

  player.find(".score").text(score);
  player.find(".graph > span").animate({ width: graph_width_pc}, ANIMATION_SPEED);
  player.animate({ top: top_px }, ANIMATION_SPEED);
}

function create_player(player, name)
{
  var e_player = $("<div/>", {
    id: "player["+player+"]",
    class: "row player",
  });

  var e_name = $("<div/>", {
    class: "span3 name",
    text: name
  });
  e_player.append(e_name);

  var e_graph = $("<div/>", {
    class: "span8 graph"
  });
  var e_graph_span = $("<span/>", {
    style: "width: 0%"
  });
  e_graph.append(e_graph_span);
  e_player.append(e_graph);

  var e_score = $("<div/>", {
    class: "span1 score",
    text: "0"
  });
  e_player.append(e_score);
  e_player.css("top", $(document).height() + "px");

  $("#players").append(e_player);
  return e_player;
}
