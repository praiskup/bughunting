#!/bin/bash

set -x

devdir="$(readlink -f ..)"

rm -rf /usr/lib/ruby/site_ruby/1.8/bughunting
ln -s $devdir/src/bughunting /usr/lib/ruby/site_ruby/1.8/

rm -f /usr/bin/huntserver
ln -s $devdir/src/huntserver.rb /usr/bin/huntserver

rm -f /usr/bin/huntlocal
ln -s $devdir/src/huntlocal.rb /usr/bin/huntlocal

rm -f /usr/bin/hunt
ln -s $devdir/src/hunt.rb /usr/bin/hunt

rm -f /etc/bughunting/*
mkdir -p /etc/bughunting
ln -s $devdir/config/server.yml /etc/bughunting/
ln -s $devdir/config/client.yml /etc/bughunting/
ln -s $devdir/config/remote_id_rsa /etc/bughunting/

rm -rf /usr/share/bughunting/tasks
mkdir -p /usr/share/bughunting
ln -s $devdir/tasks /usr/share/bughunting/tasks
